class AddMediumToTorrent < ActiveRecord::Migration[5.0]
  def change
    add_reference :torrents, :medium, foreign_key: true
  end
end
