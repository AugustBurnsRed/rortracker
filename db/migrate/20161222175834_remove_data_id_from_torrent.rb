class RemoveDataIdFromTorrent < ActiveRecord::Migration[5.0]
  def self.up
    remove_column :torrents, :data_id
  end
end
