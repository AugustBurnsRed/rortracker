class AddUserToTorrent < ActiveRecord::Migration[5.0]
  def change
    add_reference :torrents, :user, foreign_key: true
  end
end
