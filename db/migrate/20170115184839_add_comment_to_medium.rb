class AddCommentToMedium < ActiveRecord::Migration[5.0]
  def change
    add_reference :comments, :medium, foreign_key: true
  end
end
