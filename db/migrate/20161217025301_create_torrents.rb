class CreateTorrents < ActiveRecord::Migration[5.0]
  def change
    create_table :torrents do |t|
      t.string :title
      t.text :body

      t.timestamps
    end
  end
end
