class Medium < ApplicationRecord
  has_many :bookmarks
  has_many :comments, dependent: :destroy
  has_many :torrents
end
