class ApplicationController < ActionController::Base
  include Clearance::Controller
  before_action :require_login
  before_action do
    Tmdb::Api.key("cb53af49a5a59569b27e65843208d476")
  end
  protect_from_forgery with: :exception

  add_flash_types :success, :warning, :danger, :info
end
