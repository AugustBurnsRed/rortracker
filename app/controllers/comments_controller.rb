class CommentsController < ApplicationController
  def create
    @medium = Medium.find(params[:medium_id])
    @comment = @medium.comments.create({
      :user_id => current_user.id,
      :body =>params["comment"]["body"]
    })
    redirect_to medium_path(@medium)
  end

  def destroy
    @medium = Medium.find(params[:medium_id])
    @comment = @medium.comments.find(params[:id])
    @comment.destroy
    redirect_to medium_path(@medium)
  end

end
