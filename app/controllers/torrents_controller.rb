class TorrentsController < ApplicationController

  def index
    @q = Torrent.ransack(params[:q])
    @torrents = @q.result.page(params[:page]).per(25)
  end

  def show
    @torrent = Torrent.find(params[:id])
  end

  def new
    @torrent = Torrent.new

    if params[:medium_id]
      @medium = Medium.find(params[:medium_id])
    else
      @medium = ''
    end
  end

  def edit
    @torrent = Torrent.find(params[:id])
  end

  def create
    search = Tmdb::Movie.find(params['torrent']['title'])

    medium = Medium.where(
      title: search[0].title,
      body: search[0].overview,
      poster: 'https://image.tmdb.org/t/p/w300_and_h450_bestv2' + search[0].poster_path,
      api_id: search[0].id
    ).first_or_create

    if medium.id
      @torrent = Torrent.create({
        :title => 'test titre',
        :body => params['torrent']['body'],
        :medium_id => medium.id,
        :user_id => current_user.id
      })

      redirect_to medium_path(medium)
    end
  end

  def update
		@torrent = Torrent.find(params[:id])

		if @torrent.update(torrent_params)
			redirect_to @torrent
		else
			render 'edit'
		end
	end

  def destroy
		@torrent = Torrent.find(params[:id])
		@torrent.destroy

		redirect_to torrents_path
	end

  private
    def torrent_params
      params.require(:torrent).permit(:title, :body)
    end
end
