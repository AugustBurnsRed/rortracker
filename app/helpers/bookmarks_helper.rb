module BookmarksHelper
  # switch between add and remove bookmark
  def bookmark_link(bookmark = '')
    if bookmark.present?
      link_to 'Remove bookmark', bookmark, method: :delete
    else
      link_to "Bookmark", bookmarks_path(:user_id => current_user.id, :medium_id => @medium.id), :method => :post
    end
  end
end
