Rails.application.routes.draw do
  # clearance
  resources :passwords, controller: "clearance/passwords", only: [:create, :new]
  resource :session, controller: "clearance/sessions", only: [:create]

  resources :users, controller: "clearance/users", only: [:create] do
    resource :password,
      controller: "clearance/passwords",
      only: [:create, :edit, :update]
  end

  get "/sign_in" => "clearance/sessions#new", as: "sign_in"
  delete "/sign_out" => "clearance/sessions#destroy", as: "sign_out"
  get "/sign_up" => "clearance/users#new", as: "sign_up"

  # thredded
  mount Thredded::Engine => '/forum'

  # bookmarks
  resources :bookmarks

  # media
  resources :media do
    resources :comments
  end

  # torrent
  resources :torrents

  # user
  resources :users



  # home page
  root 'welcome#index'
end
