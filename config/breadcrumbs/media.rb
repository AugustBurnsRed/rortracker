crumb :media do
   link "Media", media_path
end

crumb :medium do |medium|
  link medium.title, medium_path(medium)
  parent :media
end
