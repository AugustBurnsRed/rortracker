crumb :users do
  link "Users", users_path
end

crumb :user do |user|
  link user.username, user_path(user)
  parent :users
end
